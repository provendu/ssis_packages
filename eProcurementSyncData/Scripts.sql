
/****** Object:  Schema [sl]    Script Date: 07/09/2020 16:36:26 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'sl')
EXEC sys.sp_executesql N'CREATE SCHEMA [sl]'
GO
/****** Object:  Table [sl].[CategoriesOfItems]    Script Date: 07/09/2020 16:36:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sl].[CategoriesOfItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [sl].[CategoriesOfItems](
	[Code] [nvarchar](50) NULL,
	[Description] [nvarchar](250) NULL,
	[ParentCode] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Active] [bit] NOT NULL,
	[SmallPictureName] [nvarchar](100) NULL,
	[LargePictureName] [nvarchar](100) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [sl].[Customers]    Script Date: 07/09/2020 16:36:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sl].[Customers]') AND type in (N'U'))
BEGIN
CREATE TABLE [sl].[Customers](
	[CustomerCode] [nvarchar](50) NULL,
	[Name] [nvarchar](250) NULL,
	[FullCustomerCode] [nvarchar](100) NULL,
	[Address] [nvarchar](150) NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [sl].[Items]    Script Date: 07/09/2020 16:36:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sl].[Items]') AND type in (N'U'))
BEGIN
CREATE TABLE [sl].[Items](
	[Code] [nvarchar](50) NULL,
	[Description] [nvarchar](250) NULL,
	[Description2] [nvarchar](250) NULL,
	[Active] [bit] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[SmallPictureName] [nvarchar](100) NULL,
	[LargePictureName] [nvarchar](100) NULL,
	[YoutubeVideoCode] [nvarchar](50) NULL,
	[CatalogCode] [nvarchar](50) NULL,
	[CategoryOfItemCode] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [sl].[SLConnectedItems]    Script Date: 07/09/2020 16:36:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sl].[SLConnectedItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [sl].[SLConnectedItems](
	[ConnectedItemId] [int] NOT NULL,
	[MainItemId] [int] NOT NULL,
	[DirectionCode] [nvarchar](10) NOT NULL,
	[ModifiedDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [sl].[SLDirection]    Script Date: 07/09/2020 16:36:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[sl].[SLDirection]') AND type in (N'U'))
BEGIN
CREATE TABLE [sl].[SLDirection](
	[SLDirectionID] [int] NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[FinancialEntityID] [int] NOT NULL,
	[SortIndex] [int] NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[AuditID] [int] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[Active] [bit] NOT NULL
) ON [PRIMARY]
END
GO
